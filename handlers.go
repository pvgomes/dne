// Copyright 2013 Zocprint Serviços Gráficos Ltda. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"bitbucket.org/zocprint/dne/schema"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

const (
	CEP_URL       = `/cep/{cep:\d{8}}`
	QUICK_CEP_URL = `/quick/cep/{cep:\d{8}}`
)

func setResponse(resp http.ResponseWriter, statusCode int, mimeType string, body interface{}) {
	resp.Header().Set("Content-Type", mimeType)
	resp.WriteHeader(statusCode)
	fmt.Fprintln(resp, body)
}

func cepHandler(resp http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	if err := req.Body.Close(); err != nil {
		setResponse(resp, http.StatusInternalServerError, "text/plain", "Error closing request body")
		return
	}

	if _, ok := vars["cep"]; !ok {
		setResponse(resp, http.StatusPreconditionFailed, "text/plain", "Missing parameter")
		return
	}

	cep, err := strconv.Atoi(vars["cep"])
	if err != nil {
		setResponse(resp, http.StatusNotAcceptable, "text/plain", "Invalid parameter")
		return
	}

	obj := schema.SearchCEP(uint32(cep))
	if obj == nil {
		setResponse(resp, http.StatusNotFound, "text/plain", "Not found")
		return
	}

	js, err := json.Marshal(obj)
	if err != nil {
		setResponse(resp, http.StatusInternalServerError, "text/plain", "Error when marshalling json")
		return
	}

	setResponse(resp, http.StatusOK, "text/json; charset=utf-8", string(js))
}

func quickCEPHandler(resp http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	if err := req.Body.Close(); err != nil {
		setResponse(resp, http.StatusInternalServerError, "text/plain", "Error closing request body")
		return
	}

	if _, ok := vars["cep"]; !ok {
		setResponse(resp, http.StatusPreconditionFailed, "text/plain", "Missing parameter")
		return
	}

	cep, err := strconv.Atoi(vars["cep"])
	if err != nil {
		setResponse(resp, http.StatusNotAcceptable, "text/plain", "Invalid parameter")
		return
	}

	obj := schema.GetQuickCEP(uint32(cep))
	if obj == nil {
		setResponse(resp, http.StatusNotFound, "text/plain", "Not found")
		return
	}

	js, err := json.Marshal(obj)
	if err != nil {
		setResponse(resp, http.StatusInternalServerError, "text/plain", "Error when marshalling json")
		return
	}

	setResponse(resp, http.StatusOK, "text/json; charset=utf-8", string(js))
}
